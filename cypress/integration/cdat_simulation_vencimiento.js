
import simulations from '../fixtures/simulation_scenarios_vencimiento.json';
import {documentTypeHtmlSelection} from '../support/functions/cdat_functions.js'
import {addfechaPeridosTotal} from '../support/functions/formulas.js'

simulations.forEach(simulation => {

context(`Test case: Simulacion - ${simulation.input_title_test} - ${simulation.input_jira_tiket}`, () => {
    before(() => {
        cy.visit('https://www-qa.bancofalabella.com.co/',{timeout:40000})
    })

        describe('Home page', () => {
            it('should home page', () => {
                cy.verifyHomeValidation()
            })
    
        })

        describe('cdat page from ', () => {
        
            it(`Verify cdat form ${simulation.input_cdat_type} `, () => {
                cy.verifyFormCdatValidation()
            })
        })

        describe('Fill Cdat form', () => {
            it('Fill form',() => {
    
              cy.fillCdatForm(documentTypeHtmlSelection(simulation.input_document_type),simulation.input_document_number,simulation.input_amount)
    
            }) 
        })


        describe('investment days', () => {
            it('days',() => {
                cy.vencimientoInputCalendar(addfechaPeridosTotal(simulation.input_days,'YYYY-MM-DD'))
            }) 
        })

        describe('Send Cdat form', () => {
            it('Send form',() => {
                cy.summitCdat(simulation.input_jira_tiket)
            }) 
        })


            describe('Verify Cdat Result', () => {
                it('Cdat Result',() => {
                    if (simulation.input_amount < 200000 ||  simulation.input_amount > 999999999){
                        cy.amountValidationError(simulation.input_amount,simulation.input_jira_tiket);  
                    }else{
                        cy.verifyResult(simulation,simulation.input_jira_tiket)
                    }
                   
                }) 
            })
    })

});
