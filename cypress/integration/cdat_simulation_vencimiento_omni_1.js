
import simulations from '../fixtures/simulation_scenarios_omni_1.json';
import {documentTypeHtmlSelection} from '../support/functions/cdat_functions.js'
import {addfechaPeridosTotal} from '../support/functions/formulas.js'

simulations.forEach(simulation => {

context(`Test case: Simulacion - ${simulation.input_title_test} - ${simulation.input_jira_tiket}`, () => {
    before(() => {
        cy.visit('https://www-qa.bancofalabella.com.co/',{timeout:40000})
    })
    beforeEach(() => {
        cy.intercept(`${'https://web-qa.bancofalabella.com.co/'}**`, req => {
            req.headers['x-site'] = 'opalpha'
    
            // or to delete a header
            //delete req.headers['Id']
        })
    })


    describe('Home page', () => {
        it('should home page', () => {
            cy.verifyHomeValidationOmni1(simulation.input_document_type,simulation.input_document_number,simulation.input_password)
        })

    })
    
    describe('cdat page from Omni 1', () => {
        it(`Verify cdat form Expiration Omni 1 `, () => {
            cy.verifyFormCdatValidationOmni1()
        })
    })

    describe('investment Expiration Omni 1', () => {
        it('days selection',() => {
            cy.expirationValidationOmni1();
            cy.vencimientoInputCalendar(addfechaPeridosTotal(simulation.input_days,'YYYY-MM-DD'));
            cy.fillCdatFormOmni1(simulation.input_amount,simulation.input_jira_tiket)
          
        }) 
    })

    describe('Verify Cdat Expiration Result', () => {
        it('Cdat Result',() => {
            if (simulation.input_amount < 200000 ||  simulation.input_amount > 999999999){
                cy.amountValidationErrorOmni1(simulation.input_amount,simulation.input_jira_tiket);  
            }else{
                cy.verifyResultExpirationOmni1(simulation,simulation.input_jira_tiket)
            }
           
        }) 
    })
      
})

});
