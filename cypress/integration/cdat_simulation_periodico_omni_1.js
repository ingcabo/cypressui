
import simulations from '../fixtures/simulation_scenarios_omni_1.json';
import {documentTypeHtmlSelection,selectorBytext} from '../support/functions/cdat_functions.js'

simulations.forEach(simulation => {
let flag = true

context(`Test case: Simulacion - ${simulation.input_title_test} - ${simulation.input_jira_tiket}`, () => {
    before(() => {
        cy.visit({ 
            url: "https://www-qa.bancofalabella.com.co/", 
            headers: { 
              "foo": "bar" 
            } 
          }) 
    })
    beforeEach(() => {
        cy.intercept(`${'https://web-qa.bancofalabella.com.co/'}**`, req => {
            req.headers['x-site'] = 'opalpha'
    
            // or to delete a header
            //delete req.headers['Id']
        }).as('Head-omni1')
    })

    describe('Home page', () => {
        it('should home page', () => {
            cy.verifyHomeValidationOmni1(simulation.input_document_type,simulation.input_document_number,simulation.input_password)
        })

    })

    describe('cdat page from Omni 1', () => {
        it(`Verify cdat form Periodico Omni 1 `, () => {
            cy.verifyFormCdatValidationOmni1()
        })
    })

    describe('investment Periodic Omni 1', () => {
        it('days selection',() => {
            cy.periodicValidationOmni1();
            cy.periodicInputDays(simulation.input_days);
            cy.fillCdatFormOmni1(simulation.input_amount,simulation.input_jira_tiket).then((val) => {
                if (!val) {
                 flag = val
                }
              })
          
        }) 
    })
    describe('Verify Cdat Result Omni 1', () => {
        it('Cdat Result',() => {
            if (flag){
                cy.verifyResultPeriodicOmni1(simulation,simulation.input_jira_tiket)
            }
        }) 
    })

    describe('Invest Cdat Result Omni 1', () => {
        it('Cdat Result - Invest',() => {
            cy.investmentStepsOmni1()
        }) 
    })
        
    })


});
