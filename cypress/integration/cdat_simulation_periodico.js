
import simulations from '../fixtures/simulation_scenarios_vencimiento.json';
import {addfechaPeridosTotal,documentTypeHtmlSelection} from '../support/functions/cdat_functions.js'

simulations.forEach(simulation => {
let flag = true
context(`Test case: Simulacion - ${simulation.input_title_test} - ${simulation.input_jira_tiket}`, () => {
    before(() => {
        cy.visit({ 
            url: "https://www-qa.bancofalabella.com.co/", 
            headers: { 
              "x-foo-baz": "bar-quux" 
            } 
          }) 
    })

        describe('Home page', () => {
            it('should home page', () => {
                cy.verifyHomeValidation()
            })
    
        })

        describe('cdat page from ', () => {
        
            it(`Verify cdat form Periodico `, () => {
                cy.verifyFormCdatValidation()
            })
        })

        describe('investment Periodic', () => {
            it('days selection',() => {
                cy.periodicValidation();
                cy.periodicInputDays(simulation.input_days);
              
            }) 
        })

        describe('Fill Cdat form', () => {
            it('Fill form',() => {
              cy.fillCdatForm(documentTypeHtmlSelection(simulation.input_document_type),simulation.input_document_number,simulation.input_amount,simulation.input_jira_tiket)
    
            }) 
        })


        describe('Send Cdat form', () => {
            it('Send form',() => {
                cy.summitCdat(simulation.input_jira_tiket).then((val) => {
                    
                    if (!val) {
                      // dismiss the wizard conditionally by enqueuing these
                      // three additional commands
                     flag = val
                     cy.log("Boton deshabilitado por falta de datos en el formulario.")
                    }
                  })
                
            }) 
        })

        describe('Verify Cdat Result', () => {
            it('Cdat Result',() => {
            if(flag){
                
                cy.verifyResultPeriodic(simulation,simulation.input_jira_tiket)
                
            }
                
            }) 
        })
    
        
    })


});
