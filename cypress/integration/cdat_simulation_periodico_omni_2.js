
import simulations from '../fixtures/simulation_scenarios_omni_2.json';
import {documentTypeHtmlSelection,selectorBytext} from '../support/functions/cdat_functions.js'

simulations.forEach(simulation => {

context(`Test case: Simulacion - ${simulation.input_title_test} - ${simulation.input_jira_tiket}`, () => {
    before(() => {
        cy.visit({ 
            url: "https://plataforma-qa.bancofalabella.com.co/", 
            
          }) 
    })

    describe('Login page Omni 2', () => {
        it('should home page', () => {
            cy.verifyLoginValidationOmni2(simulation.input_exec_email,simulation.input_exec_password)
        })
        
    })

    //https://plataforma-qa.bancofalabella.com.co/bankingserver-api-falabella/rest/callService/json/processSystemUserLoginByCustomerMigration
    //https://plataforma-qa.bancofalabella.com.co/api/emission/ingress/branchesEmission
    
    
    /*beforeEach(() => {
        cy.intercept(`${'https://plataforma-qa.bancofalabella.com.co/'}**`, req => {
            req.headers['x-site'] = 'opalpha'
    
            // or to delete a header
            //delete req.headers['Id']
        }).as('requestApi')
    })*/
    
    describe('Home page Omni 2', () => {
        it('should home page', () => {
            
                
            
            cy.verifyHomeValidationOmni2(simulation.input_document_type,simulation.input_document_number,simulation.input_password)
        })

    })

    

   /* 
describe('cdat page from Omni 2', () => {
        it(`Verify cdat form Periodico Omni 2 `, () => {
            cy.verifyFormCdatValidationOmni2()
        })
    })
    
    

    describe('investment Periodic Omni 2', () => {
        it('days selection',() => {
            cy.periodicValidationOmni2();
            cy.periodicInputDays(simulation.input_days);
            cy.fillCdatFormOmni2(simulation.input_amount,simulation.input_jira_tiket)
          
        }) 
    })
    describe('Verify Cdat Result Omni 2', () => {
        it('Cdat Result',() => {
            if (simulation.input_amount < 200000 ||  simulation.input_amount > 999999999){
                cy.amountValidationErrorOmni2(simulation.input_amount,simulation.input_jira_tiket);  
            }else{
                cy.verifyResultPeriodicOmni2(simulation,simulation.input_jira_tiket)
            }
        }) 
    })*/
        
    })


});
