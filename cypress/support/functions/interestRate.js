
function tasaMensual(capital,days){

    let interes= null;

    if (capital >= 200000 && capital <= 99999999){
      interes = validateInteresColumnA(days);
    }else if (capital >= 100000000 && capital <= 999999999){
      interes = validateInteresColumnB(days);
    }else if (capital >= 1000000000 && capital <= 9999999999){
      interes = validateInteresColumnC(days);
    }else{
        interes= 0;
    }
    return interes;

}

function validateInteresColumnA(days){
    //amount 500.000 to 99.999.999
  let interes= 0;  
  if (days >= 30 && days <= 59){
      interes=0.02;
  }else if(days >= 60 && days <= 89){
      interes=0.12;
  }else if (days >= 90 && days <= 149){
      interes=0.56;
  }else if (days >= 150 && days <= 179){
      interes=0.56;
  }else if (days >= 180 && days <= 269){
      interes=0.62;
  }else if (days >= 270 && days <= 359){
      interes=0.62;
  }else if (days >= 360 && days <= 539){
      interes=0.71;
  }else if (days >= 540 && days <= 719){
      interes=0.73;
  }else if (days >= 720 && days <= 1079){
      interes=0.76;
  }else if (days == 1080){
      interes=0.87;
  }else{
      interes= 0;
  }
  return interes;
  
}

 function validateInteresColumnB(days){
  //amount 100.000.000 to 999.999.999
  let interes= 0;  
 if (days >= 30 && days <= 59){
      interes=0.02;
  }else if(days >= 60 && days <= 89){
      interes=0.12;
  }else if (days >= 90 && days <= 149){
      interes=0.56;
  }else if (days >= 150 && days <= 179){
      interes=0.56;
  }else if (days >= 180 && days <= 269){
      interes=0.62;
  }else if (days >= 270 && days <= 359){
      interes=0.62;
  }else if (days >= 360 && days <= 539){
      interes=0.71;
  }else if (days >= 540 && days <= 719){
      interes=0.73;
  }else if (days >= 720 && days <= 1079){
      interes=0.76;
  }else if (days == 1080){
      interes=0.87;
  }else{
      interes= 0;
  }
  return interes;
  
}

  function validateInteresColumnC(days){
  //amount 1.000.000.000 to 9.999.999.999
  let interes= 0;  
 if (days >= 30 && days <= 59){
      interes=0.02;
  }else if(days >= 60 && days <= 89){
      interes=0.12;
  }else if (days >= 90 && days <= 149){
      interes=0.56;
  }else if (days >= 150 && days <= 179){
      interes=0.56;
  }else if (days >= 180 && days <= 269){
      interes=0.62;
  }else if (days >= 270 && days <= 359){
      interes=0.62;
  }else if (days >= 360 && days <= 539){
      interes=0.71;
  }else if (days >= 540 && days <= 719){
      interes=0.73;
  }else if (days >= 720 && days <= 1079){
      interes=0.76;
  }else if (days == 1080){
      interes=0.87;
  }else{
      interes= 0;
  }
  return interes;
  
}

function tasaEfectivaAnual(rate, timePeriod =12) {
  var tasa = (100*(Math.pow(1+rate/100,timePeriod/1)-1)).toFixed(2);
  return tasa;
}


module.exports = {
    tasaMensual,
    tasaEfectivaAnual
 }