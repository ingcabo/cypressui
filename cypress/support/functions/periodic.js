import {cantidadPeriodos,agregarMesFecha,diasEntreFechas,calculateCompoundInteRestBydays,toJSONLocal}  from './formulas.js'

let capital = 350000
let days = 90
let interestRateValue = 6.93
let nDecimals= 2;
let tax = 0.04;
let today = new Date();
let fechaa =  new Date();
//fechaa.setDate(17)
//fecha1.setDate(17)
//today.setDate(17)

//let periodoUnoSimulation = montoPrimerPeriodo(capital,interestRateValue);
//let completeSimulation =loopPeriodic(days,capital,interestRateValue);


function montoPrimerPeriodo(capital,interestRateValue){
    let fecha1 = new Date();
    let today = new Date();
    let fechaPeriodo1 =agregarMesFecha(fecha1,1);
    let diasPeriodo1 = diasEntreFechas(toJSONLocal(today),toJSONLocal(fechaPeriodo1))
    let montoPrimerPeriodo = calculateCompoundInteRestBydays(capital,interestRateValue,diasPeriodo1);
    let primerTax = (montoPrimerPeriodo*tax).toFixed(nDecimals);
    montoPrimerPeriodo = (montoPrimerPeriodo) - primerTax; 
    let resumenPeriodo1=  {primerTax,montoPrimerPeriodo};
    return resumenPeriodo1
    
}

function loopPeriodic(days,capital,interestRateValue){
    let tax = 0.04;
    let today = new Date();
    let periodos = cantidadPeriodos(days);
    //today.setDate(17);
    today.setHours(0,0,0,0,0);

    let fechaInicioPeriodo ="";
    let fechaFinPeriodo="";
    let TotalAmount=0;
    let totalDias=0;
    for(var i = 1; i <= periodos; i++){
        let day = new Date();
       // day.setDate(17);
        day.setHours(0,0,0,0,0);
        //console.log("================ Periodo Numero ",i)
        if (i ==1){
            fechaInicioPeriodo =agregarMesFecha(day,0);
            //console.log("fechaInicioPeriodo",fechaInicioPeriodo)
            //fechaFinPeriodo =agregarMesFecha(day,i);
        }else{
            fechaInicioPeriodo =agregarMesFecha(day,i-1);
        }
       
         fechaFinPeriodo = agregarMesFecha(day,1);
         
         let diasFinal = diasEntreFechas(toJSONLocal(fechaInicioPeriodo),toJSONLocal(fechaFinPeriodo))
         let monto = calculateCompoundInteRestBydays(capital,interestRateValue,diasFinal);
         let taxAmount = (monto*tax).toFixed(nDecimals);
         monto = (monto - (taxAmount)).toFixed(nDecimals); 
         TotalAmount = parseFloat(TotalAmount) + parseFloat(monto);
         totalDias = diasFinal + totalDias
       
        
        console.log("fechaInicioPeriodo",fechaInicioPeriodo)
        console.log("fechaFinPeriodo",fechaFinPeriodo)
        console.log("dias del mes ===",diasFinal)
        console.log("monto al mes===",monto)
        console.log("taxAmount del mes===",taxAmount)
        console.log("================================================")

    }
    TotalAmount = TotalAmount + capital;
    console.log("================ TotalDias ==",totalDias)
    console.log("================ TotalAmount ==",TotalAmount)
    return TotalAmount
}




module.exports = {
    montoPrimerPeriodo,
    loopPeriodic
 }
