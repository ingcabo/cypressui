
function cantidadPeriodos(dias) {
    let periodos = dias/30;
    return periodos;
  }

  function agregarMesFecha(fecha,n=1){
    return new Date(fecha.setMonth(fecha.getMonth()+n));
}

function diasEntreFechasTotal(dias){
    const fechaActual =  new Date();
    const today = new Date();
    let periodos = cantidadPeriodos(dias);
    let fechaPeriodoCompleto =agregarMesFecha(fechaActual,periodos);
    let fechaNeta = toJSONLocal(fechaPeriodoCompleto);
    let diasFinal = diasEntreFechas(toJSONLocal(today),fechaNeta)
    return diasFinal;

}

function diasEntreFechas(date_1,date_2){
    date_1 = new Date(date_1);
    date_2 = new Date(date_2);
   var day_as_milliseconds = 86400000;
   var diff_in_millisenconds = date_2 - date_1;
   var diff_in_days = diff_in_millisenconds / day_as_milliseconds;
   return diff_in_days;
}

function toJSONLocal(date) {
    var local = new Date(date);
    local.setMinutes(date.getMinutes() - date.getTimezoneOffset());
    return local.toJSON().slice(0, 10);
  }

function calculateCompoundInteRestBydays(capital, rate, dia) {
    let expPart = Math.pow((1+(rate/100)),(dia/360)).toFixed(9);
    let amount = ((capital) * ((expPart)-1));
    return amount;
}


function addfechaPeridosTotal(dias,format){
    const fechaActual =  new Date();
    let periodos = cantidadPeriodos(dias);
    let fechaPeriodoCompleto =agregarMesFecha(fechaActual,periodos);
    let fechaNeta = toJSONLocal(fechaPeriodoCompleto);
    console.log(dateFunction(fechaNeta,format))
    return dateFunction(fechaNeta,format)

}

function dateFunction (myDate,format){
    let toDate = myDate.split("-");
    let date = new Date();
    let pminutes=0;
    date = new Date(date.setTime(date.getTime() + (pminutes * 60 * 1000)));
    const hour = date.getHours().toString().padStart(2, "0");
    const seconds = date.getSeconds().toString().padStart(2, "0");
    const minutes = date.getMinutes().toString().padStart(2, "0");
    const day = toDate[2];
    const month =  toDate[1];
    const year = toDate[0];
    const formattedDate = formatDate(format,year,month,day,hour,minutes,seconds);
    return formattedDate;
  };
  
  function formatDate(format,year,month,day,hour,minutes,seconds){
      switch(format) {
         case 'YYYY-MM-DD': {
            return `${year}-${month.toString().padStart(2, "0")}-${day.toString().padStart(2, "0")}`;
         }
         case 'YYYY-MM-DDT': {
            return `${year}-${month.toString().padStart(2, "0")}-${day.toString().padStart(2, "0")}T${hour}:${minutes}:${seconds}-03:00`; 
         }
        case 'MM/DD/YYYY': {
           return `${month.toString().padStart(2, "0")}/${day.toString().padStart(2, "0")}/${year}`; 
        }
        case 'DD/MM/YYYY': {
            return `${day.toString().padStart(2, "0")}/${month.toString().padStart(2, "0")}/${year}`; 
         }
        case 'MM-DD-YYYY': {
            return `${month.toString().padStart(2, "0")}-${day.toString().padStart(2, "0")}-${year}`; 
         }
         case 'YYYY-MM-DD': {
            return `${year}-${month.toString().padStart(2, "0")}-${day.toString().padStart(2, "0")}`; 
         }
         default: {
              return `${year}-${month.toString().padStart(2, "0")}-${day.toString().padStart(2, "0")}T${hour}:${minutes}:${seconds}-03:00`; 
         }
      }  
  }

  function milesformat (num) {
    var reg=/\d{1,3}(?=(\d{3})+$)/g; 
    return (num + '').replace(reg, '$&.');

}

function calculoVencimiento(days,capital,interestRateValue){
    let fecha1 = new Date();
    let today = new Date();
    let tax = 0.04;
    let revenue =0;
    let periodos = cantidadPeriodos(days);
    let nDecimals = 2

    let fechaFinalPeriodo =agregarMesFecha(fecha1,periodos);
    cy.log("today "+ today)
    cy.log("fechafinalperiodo"+ fechaFinalPeriodo)
    let diasTotales = diasEntreFechas(toJSONLocal(today),toJSONLocal(fechaFinalPeriodo))
    let amount = parseFloat(calculateCompoundInteRestBydays(capital,interestRateValue,diasTotales));
    let taxAmount = parseFloat((amount*tax).toFixed(nDecimals));
    amount = parseFloat(amount).toFixed(nDecimals) - taxAmount.toFixed(nDecimals); 
    revenue =amount + capital;
    let resumenVecimiento=  {taxAmount,amount,revenue};
    return resumenVecimiento
    
}

module.exports = {
    cantidadPeriodos,
    agregarMesFecha,
    diasEntreFechas,
    calculateCompoundInteRestBydays,
    diasEntreFechasTotal,
    toJSONLocal,
    dateFunction,
    addfechaPeridosTotal,
    milesformat,
    calculoVencimiento
 }