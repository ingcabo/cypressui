const limpiaMonto = (string)=> string.replace('$','').replace(/[.%$]/gi, '').trim();
const limpiaMontoTasa = (string)=> string.replace('$','').replace(/[%$]/gi, '').trim();

module.exports = {
    limpiaMonto,
    limpiaMontoTasa
}