
import {cantidadPeriodos,agregarMesFecha,diasEntreFechas,calculateCompoundInteRestBydays,toJSONLocal}  from './formulas.js'

let capital = 350000
let days = 90
let interestRateValue = 6.93
let nDecimals= 2;
//fechaa.setDate(17)
//fecha1.setDate(17)
//today.setDate(17)


//let vencimient = calculoVecimiento(days,capital,interestRateValue);


function calculoVecimiento(days,capital,interestRateValue){
    let fecha1 = new Date();
    let today = new Date();
    let tax = 0.04;
    let revenue =0;
    let periodos = cantidadPeriodos(days);

    let fechaFinalPeriodo =agregarMesFecha(fecha1,periodos);
    let diasTotales = diasEntreFechas(toJSONLocal(today),toJSONLocal(fechaFinalPeriodo))
    let amount = parseFloat(calculateCompoundInteRestBydays(capital,interestRateValue,diasTotales));
    let taxAmount = parseFloat((amount*tax).toFixed(nDecimals));
    amount = parseFloat(amount).toFixed(nDecimals) - taxAmount.toFixed(nDecimals); 
    revenue =amount + capital;
    let resumenVecimiento=  {taxAmount,amount,revenue};
    return resumenVecimiento
    
}