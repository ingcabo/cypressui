Cypress.Commands.add('clickOnButton',(selectorName) =>{
    cy.contains(selectorName).click();
})


Cypress.Commands.add('fillInputText',(selectorName, textValue) =>{
    cy.get(selectorName).type(textValue)
})

Cypress.Commands.add('selectorBeVisible',(selectorName, textValue) =>{
    cy.get(selectorName).contains(textValue).should('be.visible');
})



Cypress.Commands.add('fillInputTextReplaceSelectorKey',(selectorName, selectorValue, textValue) =>{
    let newstr = selectorName.replace("%%", selectorValue);
    cy.get(newstr).type(textValue)
})


Cypress.Commands.add('selectOptionContaining', (selectorName, optionTextPart) => {
    cy.get(selectorName)
    .find('option')
    .contains(optionTextPart)
    .then($option => {
        cy.get(selectorName).select($option.text());
    });
});