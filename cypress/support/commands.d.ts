// in cypress/support/index.d.ts
// load type definitions that come with Cypress module
/// <reference types="cypress" />

declare namespace Cypress {

    interface Chainable {
      /**
       * 
       * cy
       * getHomeValidation()
       */
       verifyHomeValidation()
    }
    interface Chainable {
      /**
       * 
       * cy
       * getHomeValidation()
       */
       verifyHomeValidationOmni1()
    }

   

    interface Chainable {
      /*
      *
      * @example
      * 
      * 
      */
      verifyFormCdatValidation()
    }

    interface Chainable {

      fillCdatForm()
    }

    interface Chainable {

      summitCdat()
    }
    
    interface Chainable {

      vencimientoInputCalendar()
    }

    interface Chainable {

      periodicValidation()
    }

    interface Chainable {

      periodicValidationOmni1()
    }

    interface Chainable {

      verifyResult()
    }


    interface Chainable {

      periodicInputDays()
    }

    interface Chainable {

      selectOptionContaining()
    }

    interface Chainable {

      amountValidationError()
    }

    interface Chainable {

      verifyResultPeriodic()
    }
    interface Chainable {

      verifyResultPeriodicOmni1()
    }

    interface Chainable{
      verifyFormCdatValidationOmni1()
    }

    interface Chainable{
      fillCdatFormOmni1()
    }
    interface Chainable{
      expirationValidationOmni1()
    }
    interface Chainable{
      verifyResultExpirationOmni1()
    }
    interface Chainable{
      amountValidationErrorOmni1()
    }
    interface Chainable{
      investmentStepsOmni1()
    }
    interface Chainable{
      verifyLoginValidationOmni2()
    }
    interface Chainable{
      verifyHomeValidationOmni2()
    }
    
  
    
  }
  
  
  
