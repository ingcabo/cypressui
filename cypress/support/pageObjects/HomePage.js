const TIME=9000;

class HomePage {

    
    getLegalPopUpAdvice(){
        return cy.get('.shadowLegalOver',{timeout:TIME})
    }

    getLegalPopUpAdviceCloseButton(){
        return cy.get('.dy-modal-container > .dy-modal-wrapper > .dy-modal-contents > .dy-lb-close',{timeout:TIME})
    }
 
    getGiveawaysPopUpAdvice(){
        return cy.get('.slide-in-top > app-image > picture > img',{timeout:TIME})
    }

    getGiveawaysPopUpAdviceCloseButton(){
        return cy.get('button.ng-star-inserted',{timeout:TIME})
    }

    getAlertPromotionCloseButton(){
        return cy.get('.ng-star-inserted',{timeout:TIME})
    }

    getAccessButton(){
        return cy.get('#login-button')
    }

    getIdetificationSelect(){
        return cy.get('#login-form > #selectTypeUserDesktop',{timeout:TIME})
    }

    getIdetificationInput(){
        return cy.get('#login-form > #inputUserDesktop',{timeout:TIME})
    }

    getPasswordInput(){
        return cy.get('#inputPasswordDesktop',{timeout:TIME})
    }

    getLoginButton(){
        return cy.get('#buttonLoginDesktop',{timeout:TIME})
    }

    getInsideProductsButtonMenu(){
        return cy.get('#SolicitudProducto > .menu-text > span', {timeout:TIME}).contains('Solicitud de productos')
    }

    getCdatOmni1Button(){
        return cy.get('a#cdat')
    }

    getHamburguerButton(){
        return cy.get('.col-md > .btn',{timeout:TIME})
    }

    getInvesmentButton(){
        return cy.get("#main-nav > :nth-child(3) > .nav-link",{timeout:2500}).contains('INVERSIONES')
    }
 
    getCdattButton(){
        return cy.get('#EVmQuBpfvqVsDJJXdwy51 > .d-flex > :nth-child(2) > .nav > .nav-item > .nav-link',{timeout:2500})
    }

    getDivSimulation(){
        return cy.get('div.conteiner-simulator.row',{timeout:2500})
    }

    getDPeriodicMessage(){
        return cy.get('p',{timeout:2500}).contains('Es requisito tener una Cuenta de Ahorros, PAC o Cuenta Corriente vigente y activa con Banco Falabella.')
    }

    //Login Omni 2

    getLoginFormOmni2(){
        return cy.get('.content-body')
    }
    getTitleTextFormOmni2(){
        return cy.get(':nth-child(2) > :nth-child(1) > .col-12')
    }
    getLoginFormButtonOmni2(){
        return cy.get('#login-button')
    }
    getLoginFormInputEmailOmni2(){
        return cy.get('#usernameForm')
    }
    getLoginFormInputPasswordOmni2(){
        return cy.get('#psswrdForm')
    }
    getLoginFormInputBranchesOmni2(){
        return cy.get('#branches-select')
    }
    getHomeThreeDotsOmni2(){
        return cy.get('.sizeIconVert')
    }
    getHomeTypeDocOmni2(){
        return cy.get('#document-cbx')
    }
    getHomeNumDocOmni2(){
        return cy.get('#ipt-dni')
    }
    getHomeGoButtonOmni2(){
        return cy.get('#document-button')
    }
    
}

export default new HomePage