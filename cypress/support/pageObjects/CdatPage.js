
const TIME=120000000;

class CdatPage {

    
    getCdatTitle(){
        return cy.get('.title',{timeout:TIME})
    }

    getCdatTitleOmni1(){
        return cy.get('.simulation > :nth-child(1)',{timeout:TIME})
    }
    getCdatTitleMonthlyTextOmni1(){
        return cy.get('.simulation__header > .title')
    }

    getCdatMessage(){
        return cy.get('p.message',{timeout:TIME})
    }
    getCdatMessageOmni1(){
        return cy.get('.simulation__conditions > term-deposit-informative-message > .message-container > .message')
    }


    getCdatSubtitle(){
        return cy.get('p.subtitle',{timeout:TIME})
    }

    getSpan(){
        return cy.get('span',{timeout:TIME})
    }

    getIdetificationLabel(){
        return cy.get('#label_simulation_form_identification',{timeout:TIME})
    }

    getIdetificationSelect(){
        return cy.get('#select_simulation_form_document_type',{timeout:TIME})
    }

    getIdetificationInput(){
        return cy.get('#input_simulation_form_document_number',{timeout:TIME})
    }

    getInvestTitle(){
        return cy.get('#label_simulation_form_invest_title',{timeout:TIME})
    }

    getInvestSmallInfo(){
        return cy.get('small.info',{timeout:TIME})
    }

    getSimulationInput(){
        return cy.get('#input_simulation_form_amount',{timeout:TIME})
    }

    getInterestReceiveText(){
        return cy.get('#label_simulation_form_interest_receive_text',{timeout:TIME})
    }
    
    getQuestionInfo(){
        return cy.get('.align-right-tooltip > img',{timeout:TIME})
    }

    getChooseCdatInf(){
        return cy.get('#ngb-tooltip-0',{timeout:TIME})
    }
    
    getSimulationFormExpirationButton(){
        return cy.get('label',{timeout:TIME}).contains('Al vencimiento')
    }

    getSimulationFormPeriodicButton(){
        return cy.get('label',{timeout:TIME}).contains('Abono periódico ')
    }

    getQuestionInfoDaysLabel(){
        return cy.get('#label_radio_simulation_form_expiration_periodic_time',{timeout:TIME})
    }

    getdaysRulesLabel(){
        return cy.get(':nth-child(7) > .info',{timeout:TIME})
    }

    getCalendarInput(){
        return cy.get("input[type|='date']",{timeout:TIME})
    }

    getSimulationButton(){
        return cy.get('#button_simulation_form_go_simulate',{timeout:TIME})
    }

    getDaysPeriodicSelect(){
        return cy.get('select',{timeout:TIME}).contains('Selecciona')
    }

    getsmallTermMessage(){
        return cy.get('small.simulation__disclaimer',{timeout:TIME})
    }

    getBtn(){
        return cy.get('btn',{timeout:TIME})
    }

    getLinkA(){
        return cy.get('a',{timeout:TIME})
    }

    getBtnPeriodic(){
        return cy.get(".btn-switch-right",{timeout:TIME})
    }
    getBtnExpiration(){
        return cy.get(".btn-switch-left",{timeout:TIME})
    }


    getBtnPeriodic(){
        return cy.get(".btn-switch-right",{timeout:TIME})
    }

    getLabelStaticMessage(){
        return cy.get("label.static",{timeout:TIME})
    }

    getsmallInfo(){
        return cy.get('small.info',{timeout:TIME})
    }

    getPeriodicSelectDays(){
        return cy.get('#select_radio_simulation_form_expiration_periodic_time',{timeout:TIME})
    }
    
    getAmountValidationMessage(){
        return cy.get('small.error.ng-star-inserted',{timeout:TIME})
    }
    getAmountValidationMessageOmni1(){
        return cy.get('.error',{timeout:TIME})
    }
    getAmountValidationMessage(){
        return cy.get('.error',{timeout:TIME})
    }

    getMinDaysInfoOmni1(){
        return cy.get(':nth-child(4) > .info')
    }
    
   
    
}

export default new CdatPage;
