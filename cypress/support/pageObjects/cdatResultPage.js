
const TIME=7000;

class CdatResultPage {

    
    getCdatMonthlyInterest(){
        return cy.get('span.amount',{timeout:TIME})
    }

    getCdatMonthlyInterestOmni1(){
        return cy.get('.amount')
    }

    getCdatAnualnterest(){
        return cy.get(':nth-child(5) > :nth-child(2)',{timeout:TIME})
    }

    getCdatCapitalAmount(){
        return cy.get('.simulation__details > :nth-child(3) > :nth-child(2)',{timeout:TIME})
    }

    getCdatCMonthlyAmount(){
        return cy.get('.simulation__details > :nth-child(1) > :nth-child(2)',{timeout:TIME})
    }

    getCdatCMonthlyAmount(){
        return cy.get('.simulation__details > :nth-child(1) > :nth-child(2)',{timeout:TIME})
    }

    getCdatTaxAmount(){
        return cy.get('.simulation__details > :nth-child(2) > :nth-child(2)',{timeout:TIME})
    }

    getCdatTotalAmount(){
        return cy.get('.simulation__details > :nth-child(3) > :nth-child(2)',{timeout:TIME})
    }

    getCdatInitialDate(){
        return cy.get('.simulation__details > :nth-child(6) > :nth-child(2)',{timeout:TIME})
    }

    getCdatFinalDate(){
        return cy.get('.simulation__details > :nth-child(7) > :nth-child(2)',{timeout:TIME})
    }
    //Vencimiento
    getCdatAmount(){
        return cy.get('.amount',{timeout:TIME})
    }
    getCdatTotalInvAmount(){
        return cy.get('.simulation__details > :nth-child(1) > :nth-child(2)', {timeout:TIME})
    }

    getCdatTaxAmount(){
        return cy.get('.simulation__details > :nth-child(2) > :nth-child(2)', {timeout:TIME})
    }
    getCdatAnnualEffRate(){
        return  cy.get(':nth-child(5) > :nth-child(2)', {timeout:TIME})
    }
    getCdatAnnualEffRateOmni1(){
        return  cy.get('.simulation__details > :nth-child(5) > :nth-child(2)', {timeout:TIME})
    }   

}

export default new CdatResultPage;
