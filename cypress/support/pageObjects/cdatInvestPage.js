const TIME=7000;

class CdatInvestPage {

    getInvestOmni1Button(){
        return cy.get('#button_simulation_invest', {timeout:TIME})
    }
    getInvestConfirmOmni1Button(){
        cy.get('#button_opening_confirm')
    }
    getTitleInvestOmni1(){
        return cy.get('.legend', {timeout:TIME})
    }

    getTitleQuestionsOmni1(){
        return cy.get('.sub-title', {timeout:TIME})
    }
    getQuest1Omni1(){
        return cy.get('#label_opening_is_relation_pep', {timeout:TIME})
    }
    getQuest2Omni(){
        return cy.get('#label_opening_foreign_transaction', {timeout:TIME})
    }
    getQuest3Omni1(){
        return cy.get('#label_opening_is_citizen_us', {timeout:TIME})
    }
    getQuest4Omni1(){
        return cy.get('#label_opening_long_time_us', {timeout:TIME})
    }
    getQuest5Omni1(){
        return cy.get('#label_opening_different_country_tax', {timeout:TIME})
    }
    getContinueOmni1Button(){
        return cy.get('#button_opening_confirm', {timeout:TIME})
    }
    getTitleOriginAccount(){
        return cy.get('.ng-untouched > :nth-child(1)', {timeout:TIME})
    }
    getTitleInterestPayOmni1(){
        return cy.get('.ng-untouched > :nth-child(4)', {timeout:TIME})
    }
    getErrorInactiveOriginAccountOmni1(){
        return cy.get(':nth-child(2) > .legend > term-deposit-informative-message > .message-container > .message', {timeout:TIME})
    }
    getErrorInactiveDepositAccountOmni1(){
        return cy.get(':nth-child(5) > .legend > term-deposit-informative-message > .message-container > .message', {timeout:TIME})
    }
    getTitleCheckAcceptRulesOmni1(){
        return cy.get('#label_credit_account_accept', {timeout:TIME})
    }
    getButtonNextOmni1(){
        return cy.get('#button_credit_account_next', {timeout:TIME})
    }
    getFormErrorInactiveAccount(){
        return cy.get('term-deposit-credit-account > :nth-child(1) > :nth-child(2)')
    }
        
}

export default new CdatInvestPage;
