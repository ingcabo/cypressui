import HomePage from "../pageObjects/HomePage"

Cypress.Commands.add('verifyLoginValidationOmni2',(email,password) => {

        cy.url().should('include','bancofalabella')

        HomePage.getLegalPopUpAdvice().should('be.visible')
        HomePage.getLegalPopUpAdviceCloseButton().click({force:true})

        cy.wait(1500)
        HomePage.getLoginFormOmni2().should('be.visible')
        HomePage.getTitleTextFormOmni2().should('be.visible').contains('Login Ejecutivos')
        HomePage.getLoginFormInputEmailOmni2().should('be.visible').type(email)
        HomePage.getLoginFormInputPasswordOmni2().should('be.visible').type(password)
        HomePage.getLoginFormButtonOmni2().should('be.visible').should('have.value', 'INGRESAR').click({force:true})
        HomePage.getLoginFormInputBranchesOmni2().should('be.visible')
        HomePage.getLoginFormButtonOmni2().should('be.visible').should('have.value', 'INGRESAR').click({force:true})
        cy.wait(10000)

})