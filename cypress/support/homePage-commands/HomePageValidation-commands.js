import HomePage from "../pageObjects/HomePage"

Cypress.Commands.add('verifyHomeValidation',() => {

        cy.url().should('include','bancofalabella')

        HomePage.getLegalPopUpAdvice().should('be.visible')
        HomePage.getLegalPopUpAdviceCloseButton().click({force:true})

        HomePage.getGiveawaysPopUpAdvice().should('be.visible')
        cy.wait(1500)
        HomePage.getGiveawaysPopUpAdviceCloseButton().contains('×').last().click(); 

        HomePage.getDivSimulation().should('be.visible')

        //HomePage.getHamburguerButton().should('be.visible')
        //HomePage.getHamburguerButton().click()
        cy.wait(1000)
        HomePage.getInvesmentButton().should('be.visible')
        HomePage.getInvesmentButton().click({force:true})
        cy.wait(1000)
        HomePage.getCdattButton().contains('CDAT').should('be.visible')  
        HomePage.getCdattButton().click()   

})
