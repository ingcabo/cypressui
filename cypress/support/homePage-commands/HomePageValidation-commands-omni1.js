import HomePage from "../pageObjects/HomePage"

Cypress.Commands.add('verifyHomeValidationOmni1',(documentType,documentNumber,password) => {

        cy.url().should('include','bancofalabella')

        HomePage.getLegalPopUpAdvice().should('be.visible')
        HomePage.getLegalPopUpAdviceCloseButton().click({force:true})

        HomePage.getGiveawaysPopUpAdvice().should('be.visible')
        cy.wait(1500)
        HomePage.getGiveawaysPopUpAdviceCloseButton().contains('×').last().click(); 

        HomePage.getDivSimulation().should('be.visible')

        //HomePage.getHamburguerButton().should('be.visible')
        //HomePage.getHamburguerButton().click()
        cy.wait(1000)
        HomePage.getAccessButton().should('be.visible')
        HomePage.getAccessButton().click({force:true})

        HomePage.getIdetificationSelect().select(documentType)
        HomePage.getIdetificationInput().type(documentNumber)
        HomePage.getPasswordInput().type(password);
        HomePage.getLoginButton().click({force:true})
        cy.wait(3000)
        HomePage.getLegalPopUpAdvice().should('be.visible')
        HomePage.getLegalPopUpAdviceCloseButton().click({force:true})
        cy.wait(20000)
        HomePage.getInsideProductsButtonMenu().should('be.visible')
       
        HomePage.getInsideProductsButtonMenu().realHover('mouse')
        cy.wait(1000)
        //HomePage.getInsideProductsButtonMenu().click({force:true})
        HomePage.getCdatOmni1Button().should('be.visible')
        HomePage.getCdatOmni1Button().click({force:true})         

})