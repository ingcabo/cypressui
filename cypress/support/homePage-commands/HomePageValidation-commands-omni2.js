import HomePage from "../pageObjects/HomePage"

Cypress.Commands.add('verifyHomeValidationOmni2',(documentType,documentNumber,password) => {
        
        cy.url().should('include','bancofalabella')

        HomePage.getHomeThreeDotsOmni2().should('be.visible').click({force:true})
        HomePage.getHomeTypeDocOmni2().should('be.visible').select(documentType)
        HomePage.getHomeNumDocOmni2().should('be.visible').type(documentNumber)
        HomePage.getHomeGoButtonOmni2().should('be.visible').click({force:true})
        cy.intercept(`${'https://plataforma-qa.bancofalabella.com.co/'}**`, req => {
                req.headers['x-site'] = 'opalpha'
        
                // or to delete a header
                //delete req.headers['Id']
            }).as('requestApi')

        cy.wait(10000)
        HomePage.getInsideProductsButtonMenu().should('be.visible').realHover('mouse')
        
        
        //HomePage.getCdatOmni2Button().should('be.visible').click({force:true})    


        /*HomePage.getDivSimulation().should('be.visible')

        //HomePage.getHamburguerButton().should('be.visible')
        //HomePage.getHamburguerButton().click()
        cy.wait(1000)
        HomePage.getAccessButton().should('be.visible')
        HomePage.getAccessButton().click({force:true})

        HomePage.getIdetificationSelect().select(documentType)
        HomePage.getIdetificationInput().type(documentNumber)
        HomePage.getPasswordInput().type(password);
        HomePage.getLoginButton().click({force:true})
        cy.wait(3000)
        HomePage.getLegalPopUpAdvice().should('be.visible')
        HomePage.getLegalPopUpAdviceCloseButton().click({force:true})
        cy.wait(20000)
        HomePage.getInsideProductsButtonMenu().should('be.visible')
       
        HomePage.getInsideProductsButtonMenu().realHover('mouse')
        cy.wait(1000)
        //HomePage.getInsideProductsButtonMenu().click({force:true})
        HomePage.getCdatOmni2Button().should('be.visible')
        HomePage.getCdatOmni2Button().click({force:true})         
*/
})