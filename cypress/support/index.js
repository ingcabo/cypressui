// ***********************************************************
// This example support/index.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import './commands'
import './cdatCommands/cdatValidation-commands'
import './homePage-commands/HomePageValidation-commands'
import './homePage-commands/HomePageValidation-commands-omni1'
import './homePage-commands/HomePageValidation-commands-omni2'
import './cdatCommands/cdatInputForm-commands'
import './cdatCommands/cdatInputForm-commands-omni-1'
import './cdatCommands/cdatsubmit'
import './cdatCommands/cdatInputVencimiento-commands'
import './cdatCommands/cdatValidatioPeriodic-commands'
import './cdatCommands/cdatValidatioPeriodic-commands-omni-1'
import './cdatCommands/cdatValidatioExpiration-commands-omni-1'
import './cdatCommands/cdatResultValidation-commands'
import './cdatCommands/cdatInputPeriodicDays-commands'
import './functions/simpleSelectorCommands'
import './cdatCommands/cdatAmountErrorValidation-commands'
import './cdatCommands/cdatAmountErrorValidation-commands-omni-1'
import './cdatCommands/cdatResultValidation-commands_Periodic'
import './cdatCommands/cdatResultValidation-commands_Periodic-omni-1'
import './cdatCommands/cdatResultValidation-commands_Expiration-omni-1'
import './cdatCommands/cdatValidation-commands-omni-1'
import './homePage-commands/LoginPageValidation-commands-omni2'
import './cdatCommands/cdatInvestSteps-omni-1'
import "cypress-real-events/support"



// Alternatively you can use CommonJS syntax:
// require('./commands')
