import CdatPage from "../pageObjects/CdatPage"

Cypress.Commands.add('periodicValidationOmni1',() =>{

    CdatPage.getBtnPeriodic().click({force:true})
    cy.wait(1000)
    CdatPage.getCdatMessage().contains('Es requisito tener una Cuenta de Ahorros, PAC o Cuenta Corriente vigente y activa con Banco Falabella.').should('be.visible');
    CdatPage.getLabelStaticMessage().contains('¿En qué plazo de días?').should('be.visible')
    CdatPage.getsmallInfo().contains('Mínimo 30 días - Máximo 1080 días').should('be.visible')

})