import CdatPage from "../pageObjects/CdatPage"
import CdatResultPage from "../pageObjects/cdatResultPage"
import {montoPrimerPeriodo,loopPeriodic} from '../functions/periodic.js'
import {tasaMensual,tasaEfectivaAnual} from '../functions/interestRate.js'
import {addfechaPeridosTotal,diasEntreFechasTotal,milesformat}  from '../functions/formulas.js'


Cypress.Commands.add('verifyResultPeriodicOmni1',(simulation,fileName) => {
        let variation=2;
        let days = diasEntreFechasTotal(simulation.input_days);  
        let interesMensual = tasaMensual(simulation.input_amount,simulation.input_days);
        let interesAnual = tasaEfectivaAnual(interesMensual);
        let capitalFormat = milesformat(simulation.input_amount);
        let {primerTax, montoPrimerPeriodo:montoperiodo1} = montoPrimerPeriodo(simulation.input_amount,interesAnual);
        let myTotalAmount = loopPeriodic(days,simulation.input_amount,interesAnual)
        

        cy.log("montoperiodo1 =====",montoperiodo1)
        cy.log("primerTax =====",primerTax)
        cy.log("myTotalAmount =====",myTotalAmount)

        let fechaInitial = addfechaPeridosTotal(0,"DD/MM/YYYY");
        let fechaFinal =addfechaPeridosTotal(simulation.input_days,"DD/MM/YYYY");

        cy.url().should('include','bancofalabella')
        cy.wait(7000)
        CdatPage.getCdatTitleOmni1().contains('Resultado de la simulación').should('be.visible')
        CdatPage.getCdatTitleMonthlyTextOmni1().contains('Tasa de interés a recibir mensual').should('be.visible')
        cy.log("interes mensual =====",interesMensual)
        CdatResultPage.getCdatMonthlyInterestOmni1().contains(`${interesMensual}%`).should('be.visible')
        CdatPage.getCdatSubtitle().contains(`en un plazo de ${days} días`).should('be.visible')

        CdatPage.getSpan().contains('Retención en la fuente').should('be.visible')
        CdatPage.getSpan().contains('Valor de la inversión').should('be.visible')
        CdatPage.getSpan().contains('Pago de rendimientos').should('be.visible')
        CdatPage.getSpan().contains('Tasa efectiva anual').should('be.visible')
        CdatPage.getSpan().contains('Fecha inicial de la inversión').should('be.visible')
        CdatPage.getSpan().contains('Fecha de vencimiento').should('be.visible')
        CdatResultPage.getCdatAnualnterest().contains(`${interesAnual}%`).should("be.visible")
        CdatResultPage.getCdatCapitalAmount().contains(`$ ${capitalFormat}`).should("be.visible")
        CdatPage.getSpan().contains('Abono periódico').should('be.visible');

        CdatResultPage.getCdatCMonthlyAmount().then(function($elem) {
                let monthlyAmount = $elem.text().replace('$','').replace('.','').trim();
                let valMontoPeriodo = parseInt(montoperiodo1, 10);
                let min = valMontoPeriodo - variation;
                let max = valMontoPeriodo + variation;
                expect(Number(monthlyAmount)).to.be.within(min, max);
        })


        CdatResultPage.getCdatTaxAmount().then(function($elem) {
                let taxAmount = $elem.text().replace('$','').replace('.','').trim();
                let min = (Number(primerTax)-variation);
                let max =  (Number(primerTax)+variation);
                expect(Number(taxAmount)).to.be.within(min, max);
        })

        CdatResultPage.getCdatTotalAmount().then(function($elem) {
                let totalAmount = $elem.text().replace('$','').replace(/\./g, '').trim();
                expect(Number(totalAmount)).equal(simulation.input_amount);
        })


        CdatResultPage.getCdatInitialDate().then(function($elem) {
                let initialDate = $elem.text().trim();
                expect(initialDate).to.be.equal(fechaInitial);
        })

        CdatResultPage.getCdatFinalDate().then(function($elem) {
                let finalDate = $elem.text().trim();
                expect(finalDate).to.be.equal(fechaFinal);
        })
        
        

        CdatPage.getsmallTermMessage().contains('Simulación válida solo por el día de hoy').should('be.visible')
        CdatPage.getCdatMessageOmni1().contains('¿Solo tienes activa tu Tarjeta CMR? Tranquilo, puedes abrir tu Cuenta Ahorro Costo $0 en minutos a través de la App o abrir tu CDAT en efectivo o cheque en tu oficina más cercana.').should('be.visible')
        
        cy.wait(5000)
        cy.log("veremos el scroll")
        cy.get('.simulation > :nth-child(1)').scrollIntoView()
        cy.get("div.simulation").screenshot('Result_'+fileName,{capture:"fullPage"})
        
        


     
     //   CdatPage.getQuestionInfo().trigger('mouseover')

  


})