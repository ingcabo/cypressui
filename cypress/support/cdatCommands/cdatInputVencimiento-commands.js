import CdatPage from "../pageObjects/CdatPage"

Cypress.Commands.add('vencimientoInputCalendar',(date) =>{

    CdatPage.getSimulationFormExpirationButton().click()
    CdatPage.getCalendarInput().click({force:true})
    CdatPage.getCalendarInput().type(date)
})