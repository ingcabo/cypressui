import CdatPage from "../pageObjects/CdatPage"

Cypress.Commands.add('verifyFormCdatValidationOmni1',() => {

        cy.url().should('include','bancofalabella')
        

        cy.wait(5000)
        CdatPage.getCdatTitle().contains('Simula tu CDAT').should('be.visible')
        CdatPage.getInvestTitle().contains('¿Cuánto quieres invertir?').should('be.visible')
        CdatPage.getInvestSmallInfo().contains('Monto mínimo $ 200.000').should('be.visible')
        CdatPage.getSimulationInput().should('be.visible')
        CdatPage.getSimulationFormExpirationButton().should('be.visible')
        CdatPage.getSimulationFormPeriodicButton().should('be.visible')
        CdatPage.getQuestionInfo().should('be.visible')
        CdatPage.getQuestionInfo().trigger('mouseover')
        CdatPage.getQuestionInfoDaysLabel().contains('¿En qué plazo de días?').should('be.visible')
        CdatPage.getMinDaysInfoOmni1().contains('Mínimo 30 días - Máximo 1080 días').should('be.visible')
        CdatPage.getCalendarInput().should('be.visible')
  
        cy.wait(5000)
        CdatPage.getCalendarInput().click({force:true})

})