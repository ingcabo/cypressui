import CdatPage from "../pageObjects/CdatPage"

Cypress.Commands.add('amountValidationErrorOmni1',(amount,fileName) =>{
    let error="";
    cy.url().should('include','bancofalabella')

    if (amount < 200000){
        error="Debes ingresar un monto mínimo de $ 200.000";
    }else{
        error="Debes ingresar un monto maximo de $ 999.999.999"
    }
    
   CdatPage.getAmountValidationMessageOmni1().contains(error).should('be.visible');    
   cy.get('term-deposit-simulation-form').screenshot('Negative_Amount_'+fileName,{capture:"fullPage"})
})