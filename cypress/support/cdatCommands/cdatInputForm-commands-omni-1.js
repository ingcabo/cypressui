import CdatPage from "../pageObjects/CdatPage"

Cypress.Commands.add('fillCdatFormOmni1',(amount, fileName) =>{

    cy.url().should('include','bancofalabella')

    CdatPage.getInvestTitle().scrollIntoView()
    CdatPage.getSimulationInput().type(amount);
    //CdatPage.getSimulationButton().click({force:true})
    CdatPage.getSimulationButton().then(($button) => {
        if($button.is(':disabled')){
            CdatPage.getSimulationButton().should('be.disabled').then((val) => {
            cy.log('Boton Simular desabilitado por datos incorrectos.')
            cy.get('term-deposit-simulation-form').screenshot('summitCdat_form_'+fileName,{capture:"fullPage"})
            cy.wait(1500)
            return false
          }) 
           
        }
        else{
            CdatPage.getSimulationButton().should('be.visible').click({force:true})
        }
    })
    cy.wait(1500)
})