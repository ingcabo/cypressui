import CdatPage from "../pageObjects/CdatPage"

Cypress.Commands.add('summitCdat',(fileName) =>{
    cy.get('div.page__right').screenshot('summitCdat_form_'+fileName,{capture:"fullPage"})
    cy.wait(1500)
    //CdatPage.getSimulationButton().should('be.visible').click({force:true})
    CdatPage.getSimulationButton().then(($button) => {
        if($button.is(':disabled')){
           CdatPage.getSimulationButton().should('be.disabled').then((val) => {
            return false
          }) 
           
        }
        else{
            CdatPage.getSimulationButton().should('be.visible').click({force:true})
        }
    })

})