import CdatPage from "../pageObjects/CdatPage"

Cypress.Commands.add('fillCdatForm',(documentType,documentNumber,amount,input_jira_tiket) =>{

    cy.url().should('include','bancofalabella')

    CdatPage.getInvestTitle().scrollIntoView()
    if (documentType == '') {
        cy.log('NEGATIVO Cdat al vecimiento Simulación con tipo de documento inexistente')
    }else{
        CdatPage.getIdetificationSelect().select(documentType)
    }
    CdatPage.getSimulationInput().type(amount)
    CdatPage.getIdetificationInput().type(documentNumber)
    if (amount < 200000 ||  amount > 999999999){
        cy.amountValidationError(amount,input_jira_tiket);  
    }

})