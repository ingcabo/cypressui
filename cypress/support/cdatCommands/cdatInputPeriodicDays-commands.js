import CdatPage from "../pageObjects/CdatPage"

Cypress.Commands.add('periodicInputDays',(days) =>{

    const selector = "#select_radio_simulation_form_expiration_periodic_time"
    let find = ""
    CdatPage.getPeriodicSelectDays().children('option').then(options => {
        const actual = [...options].map(o => Number(o.value.substring(o.value.indexOf(':') + 1).trim()))
        cy.log(actual) 
        cy.log(days) 
        find = actual.indexOf(days); 
        cy.log(find)  
        if(find <= 0){
            cy.log('Plazo invalido.')
        }else{
            cy.selectOptionContaining(selector,days)
        }
      })
})