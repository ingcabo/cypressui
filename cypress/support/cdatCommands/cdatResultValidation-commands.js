import CdatPage from "../pageObjects/CdatPage"
import CdatResultPage from "../pageObjects/cdatResultPage"
import formulas from '../functions/formulas.js'
import utils from '../functions/utils.js'
import interest from '../functions/interestRate.js'

Cypress.Commands.add('verifyResult',(simulation,fileName) => {
        let capital = simulation.input_amount
        let days = formulas.diasEntreFechasTotal(simulation.input_days)
        let rate = interest.tasaMensual(capital, days)
        let interestRateValue = interest.tasaEfectivaAnual(rate)
        let values = formulas.calculoVencimiento(days, capital, interestRateValue)
        cy.log("values = ", values)  
        cy.url().should('include','bancofalabella')
        cy.wait(7000)
        CdatPage.getCdatTitle().contains('¡Así puede estar creciendo tu dinero!').should('be.visible')
        //CdatPage.getCdatTitle().contains('Valor total a recibir').should('be.visible')
        CdatPage.getCdatSubtitle().contains(`en un plazo de ${days} días`).should('be.visible')
        //CdatPage.getSpan().contains('Rendimientos totales de la inversión').should('be.visible')
        CdatPage.getSpan().contains('Retención en la fuente').should('be.visible')
        CdatPage.getSpan().contains('Valor de la inversión').should('be.visible')
        CdatPage.getSpan().contains('Pago de rendimientos').should('be.visible')
        CdatPage.getSpan().contains('Tasa efectiva anual').should('be.visible')
        CdatPage.getSpan().contains('Fecha inicial de la inversión').should('be.visible')
        CdatPage.getSpan().contains('Fecha de vencimiento').should('be.visible')

        CdatPage.getsmallTermMessage().contains('Simulación válida solo por el día de hoy').should('be.visible')
       
        CdatPage.getCdatMessage().contains('Si eres Cliente activo de Banco Falabella con una Cuenta de Ahorros Costo $0, PAC o Cuenta Corriente abre tu CDAT 100% Digital desde tu Banca en línea o App.').should('be.visible')
        CdatPage.getCdatMessage().contains('¿Solo tienes activa tu Tarjeta CMR? Tranquilo, puedes abrir tu CDAT en efectivo o cheque acercándote a tu oficina más cercana.').should('be.visible')
        CdatPage.getCdatMessage().contains('¿Aún no eres Cliente de Banco Falabella? No te preocupes, puedes abrir tu Cuenta de Ahorros Costo $0 en minutos haciendo Click en el botón Crear Cuenta.').should('be.visible')
        cy.wait(5000)
        cy.log("veremos el scroll")
        cy.get('.simulation > :nth-child(1)').scrollIntoView()
        cy.get("div.simulation").screenshot('Result_'+fileName,{capture:"fullPage"})

        //Monto  Tengo que validar la formula para saber si el valor esta bien
        CdatResultPage.getCdatAmount().then(function($elem) {
                let amount =  utils.limpiaMonto($elem.text())
                let revenueAmount = parseInt(values.revenue)
                expect(Number(amount)).equal(revenueAmount)
        })
        CdatResultPage.getCdatTotalInvAmount().then(function($elem){
                let totalInvAmount = utils.limpiaMonto($elem.text())
                let amount = parseInt(values.amount)
                expect(Number(totalInvAmount)).equal(amount)

        })
        CdatResultPage.getCdatTaxAmount().then(function($elem){
                let taxAmount = utils.limpiaMonto($elem.text())
                let tax = parseInt(values.taxAmount)
                expect(Number(taxAmount)).equal(tax)

        })
        CdatResultPage.getCdatAnnualEffRate().then(function($elem){
                let rateDesired = utils.limpiaMontoTasa($elem.text())
                console.log("rateDesired = ", rateDesired)  
                let rateVal = interestRateValue
                console.log("rateVal = ", rateVal)
                expect(Number(rateDesired)).equal(Number(rateVal))

        })
        
        


     
     //   CdatPage.getQuestionInfo().trigger('mouseover')

  


})