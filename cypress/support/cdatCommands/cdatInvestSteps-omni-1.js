import CdatInvestPage from "../pageObjects/cdatInvestPage"

Cypress.Commands.add('investmentStepsOmni1',(amount, fileName) =>{

    cy.url().should('include','bancofalabella')
    cy.wait(3000)

    CdatInvestPage.getInvestOmni1Button().contains('Invertir')
    CdatInvestPage.getInvestOmni1Button().should('be.visible').click({force:true})
    cy.wait(5000)
    CdatInvestPage.getTitleInvestOmni1().contains('¡Ya casi terminamos! Unos últimos datos y listo').should('be.visible')

    CdatInvestPage.getTitleQuestionsOmni1().contains('Por temas regulatorios necesitamos hacerte estas preguntas.').should('be.visible')
    CdatInvestPage.getQuest1Omni1().contains('¿Tienes relación con una Persona Expuesta Publica y/o Políticamente (PEP)?').should('be.visible')
    CdatInvestPage.getQuest2Omni().contains('¿Realizas transacciones en moneda extranjera?').should('be.visible')
    CdatInvestPage.getQuest3Omni1().contains('¿Eres ciudadano o residente de los EE.UU?').should('be.visible')
    CdatInvestPage.getQuest4Omni1().contains('¿Has permanecido más de 182 días en el último año ó más de 121 días en promedio en los últimos 3 años, en los EE.UU?').should('be.visible')
    CdatInvestPage.getQuest5Omni1().contains('¿Eres nacionalizado o estas obligado a tributar en algún país diferente a Colombia?').should('be.visible')


    CdatInvestPage.getContinueOmni1Button().contains('Confirmar').should('be.visible').click({force:true})
    cy.wait(3000)

    CdatInvestPage.getTitleOriginAccount().contains('Selecciona tu cuenta de origen de inversion').should('be.visible')
    CdatInvestPage.getTitleInterestPayOmni1().contains('Selecciona cuenta de abono de intereses').should('be.visible')
    CdatInvestPage.getErrorInactiveOriginAccountOmni1().contains('Es requisito tener una Cuenta de Ahorros, PAC o Cuenta Corriente vigente y activa.').should('be.visible')
    CdatInvestPage.getErrorInactiveDepositAccountOmni1().contains('Es requisito tener una Cuenta de Ahorros, PAC o Cuenta Corriente vigente y activa.').should('be.visible')

    CdatInvestPage.getTitleCheckAcceptRulesOmni1().contains('Acepto el reglamento del producto CDAT.').should('be.visible')

    CdatInvestPage.getButtonNextOmni1().then(($button) => {
        if($button.is(':disabled')){
            CdatInvestPage.getButtonNextOmni1().should('be.disabled').then((val) => {
            cy.log('Boton siguiente desabilitado por cuenta inactiva.')
            CdatInvestPage.getFormErrorInactiveAccount().screenshot('summitCdat_form_'+fileName,{capture:"fullPage"})
            cy.wait(1500)
          }) 
           
        }
        else{
            CdatInvestPage.getButtonNextOmni1().should('be.visible').click({force:true})
        }
    })
    //CdatInvestPage.getButtonNextOmni1().contains('Siguiente').should('be.visible')
    


})